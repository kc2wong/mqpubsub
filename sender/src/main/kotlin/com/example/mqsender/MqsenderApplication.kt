package com.example.mqsender

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MqsenderApplication

fun main(args: Array<String>) {
	runApplication<MqsenderApplication>(*args)
}
