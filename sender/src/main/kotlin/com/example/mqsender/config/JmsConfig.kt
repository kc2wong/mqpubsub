package com.example.mqsender.config

import org.apache.activemq.ActiveMQConnectionFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jms.connection.CachingConnectionFactory
import org.springframework.jms.core.JmsTemplate
import javax.jms.DeliveryMode

@Configuration
class JmsConfig(@Value("\${activemq.broker-url}") val brokerUrl: String) {

    @Bean
    fun senderActiveMQConnectionFactory(): ActiveMQConnectionFactory? {
        val activeMQConnectionFactory = ActiveMQConnectionFactory()
        activeMQConnectionFactory.brokerURL = brokerUrl
        return activeMQConnectionFactory
    }

    @Bean
    fun cachingConnectionFactory(): CachingConnectionFactory? {
        return CachingConnectionFactory(
                senderActiveMQConnectionFactory()!!)
    }

    @Bean
    fun jmsTemplate(): JmsTemplate? {
        val jmsTemplate = JmsTemplate(cachingConnectionFactory()!!)
        jmsTemplate.isPubSubDomain = true
        jmsTemplate.deliveryMode = DeliveryMode.PERSISTENT
        jmsTemplate.setDeliveryPersistent(true)
        return jmsTemplate
    }
}