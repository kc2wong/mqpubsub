package com.example.mqsender.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.jms.core.JmsTemplate
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class MessageController(@Value("\${destination.topic}") val topicDestination: String) {

    private val logger = LoggerFactory.getLogger(MessageController::class.java)

    @Autowired
    lateinit var jmsTemplate: JmsTemplate

    @PostMapping("/message/publish")
    fun send(@RequestParam("content") message: String) : String {
        logger.info("sending message='{}' to destination='{}'", message,
                topicDestination)
        jmsTemplate.convertAndSend(topicDestination, message as Any)
        return "OK"
    }
}