package com.example.mqreceiver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MqreceiverApplication

fun main(args: Array<String>) {
	runApplication<MqreceiverApplication>(*args)
}
