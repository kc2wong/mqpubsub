package com.example.mqreceiver.config

import org.apache.activemq.ActiveMQConnectionFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jms.annotation.EnableJms
import org.springframework.jms.config.DefaultJmsListenerContainerFactory

@Configuration
@EnableJms
class JmsConfiguration(@Value("\${activemq.broker-url}") val brokerUrl: String,
                       @Value("\${activemq.subscriber-id}") val subscriberId: String) {

    @Bean
    fun receiverActiveMQConnectionFactory(): ActiveMQConnectionFactory {
        val activeMQConnectionFactory = ActiveMQConnectionFactory()
        activeMQConnectionFactory.brokerURL = brokerUrl
        return activeMQConnectionFactory
    }

    @Bean
    fun jmsListenerContainerFactory(): DefaultJmsListenerContainerFactory {
        val factory = DefaultJmsListenerContainerFactory()
        factory.setConnectionFactory(receiverActiveMQConnectionFactory()!!)
        factory.setPubSubDomain(true)
        factory.setClientId(subscriberId)
        factory.setSubscriptionDurable(true)
        return factory
    }
}