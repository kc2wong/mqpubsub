package com.example.mqreceiver.bean

import org.slf4j.LoggerFactory
import org.springframework.jms.annotation.JmsListener
import org.springframework.stereotype.Component

@Component
class Receiver() {
    private val logger = LoggerFactory.getLogger(Receiver::class.java)

    @JmsListener(destination = "\${destination.topic}", subscription = "\${destination.topic}")
    fun receive(message: String) {
        logger.info("subscriber received message={}", message)
    }
}